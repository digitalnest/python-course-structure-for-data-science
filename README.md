## Topics Covered in Core Python

1. 	Understanding the Open source
2. 	Understanding Interpreters
3. 	Introduction to Scripting languages
4. 	Evolution of Python
5. 	Installation of Python
6. 	Getting started with Python
7. 	Creating the first python program.
8. 	Understanding the .py extension.
9. 	How to run the PYTHON PROGRAM
10.	Python Scripts execution
    1. 	Using the python command line
    2. 	Using IDLE
    3.	Using IDEs
    4.	Using a regular command prompt
11. Variables
12.	Various ways of printing
13.	General data types in python
14.	Boolean functions
15.	Operators
	1.  Arithmetic
	2.	Relational
	3.	Logical
	4.	Assignment
    5.	Membership
    6.	Identiy
    7.	Unary
16.	Doc string
17.	Accepting inputs
18.	Raw inputs and normal inputs
19.	Control Statements
20.	Sequence control
21.	Decision control
22. if(pure if)
23.	if-else
24.	if-elif
25.	nesting
26.	Loop control
27.	while
28.	for-else
29.	while-else
30.	break, continue and pass keywords
31.	Indexing and slicing

---
## For more info on Python Courses

You can Check for more info about [python course for Datasciene](https://www.digitalnest.in/python-course-training-in-hyderabad/).